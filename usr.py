
"""A user is defined by a name, a email and a job.
Users are stored onto a Redis server and can be created
using the User.create class method and retrieved using the
User.from_name class method.

>>> new_user = User.create("Bob", "bob@company.com", "developer")
>>> new_user.name, new_user.email, new_user.job
('Bob', 'bob@company.com', 'developer')

User.MissingUserError is raised when trying to get a user
who doesn't exist on the Redis server.
"""
import json
import redis
from error import MissingUserError


class User(object):

    def __init__(self, name, email, job):
        """Initialize a user.

        Args:
            name (str)
            email (str)
            job (unicode)
        """
        self.__name = name
        self.__email = email
        self.__job = job

    @classmethod
    def from_name(cls, user_name):
        """Returns the unique user from the given name.

        Args:
            user_name (str)

        Returns:
            User
        """
        r = redis.Redis()
        redis_data = r.get("usr" + user_name)

        if redis_data is None:
            raise MissingUserError("No user named {}".format(user_name))

        dict_data = json.loads(redis_data)
        return cls(user_name, **dict_data)

    @classmethod
    def create(cls, name, email, job):
        """Creates a new user with the given inputs.

        Args:
            name (str)
            email (str)
            job (str)

        Returns:
            User
        """
        data = {'email': email, 'job': job}
        redis.Redis().set("usr" + name, json.dumps(data), nx=True)

        return cls(name, email, job)

    @property
    def name(self):
        """Returns the user name.

        Returns:
            str
        """
        return self.__name

    @property
    def email(self):
        """Returns th user email.

        Returns:
            str
        """
        return self.__email

    @email.setter
    def email(self, value):
        """Sets the user email.

        Args:
            value (str)
        """
        assert isinstance(value, str), (type(value), value)

        data = {'email': value, 'job': self.__job}
        redis.Redis().set("usr" + self.__name, json.dumps(data))
        self.__email = value

    @property
    def job(self):
        """Returns the user job.

        Returns:
            str
        """
        return self.__job

    @job.setter
    def job(self, value):
        """Sets the user job.

        Args:
            value (str)
        """
        assert isinstance(value, str), (type(value), value)

        data = {'email': self.__email, 'job': value}
        redis.Redis().set("usr" + self.__name, json.dumps(data))

        self.__job = value


if __name__ == '__main__':

    # u = User.create("Bob", "bob@company.com", "manager")
    # print u.name, u.email, u.job

    u = User.from_name("Bob")
    print (u.name, u.email, u.job)
