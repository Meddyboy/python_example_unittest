from unittest import TestCase
from usr import User
from error import MissingUserError
from mock import patch

class UserTestCase(TestCase):

    def test_init(self):
        # Test case for the initialization of a User object
        u = User("Test", "test@test.com", "testjob")
        self.assertIsInstance(u, User)

    def test_properties(self):
        # Test case for checking the properties of a User object
        u = User("Test", "test@test.com", "testjob")
        self.assertIsInstance(u.name, str)
        self.assertIsInstance(u.email, str)
        self.assertIsInstance(u.job, str)
        self.assertEqual(u.name, "Test")
        self.assertEqual(u.email, "test@test.com")
        self.assertEqual(u.job, "testjob")

    @patch("redis.Redis.get")
    def test_get(self, redis_mock):
        # Mocking the Redis get method for testing
        redis_mock.return_value = None

        # Test case for attempting to get an unknown user from Redis
        with self.assertRaises(MissingUserError):
            User.from_name("Test")

        redis_mock.return_value = '{"email": "test@test.com", "job": "testjob"}'

        # Test case for getting an existing user from Redis
        u = User.from_name("Test")
        self.assertIsInstance(u, User)
        
        # Assert that the Redis get method is called twice
        self.assertEqual(2, redis_mock.call_count)
