# Projet Exemple tests unitaires avec Redis

This project implements a `User` class with features to create, retrieve, and update users in a Redis server.

## Installation

  1. Make sure you have Python installed on your system.
  2. Install the dependencies by executing the following command:

  ```bash
    'pip install redis'
  ```


## Structure du Projet

   - usr.py: Definition of the User class.
   - error.py: Definition of exceptions related to Redis errors.
   - usr_test.py: Unit test file.

## Running Tests

  ```bash
    python -m unittest usr_test.py
  ```


## Authors

- [@Meddyboy](https://gitlab.com/Meddyboy)

