
class MissingError(Exception):
    """Raised when an Redis entry is missing."""


class MissingUserError(MissingError):
    """Raised when a user entry is missing."""


class MissingUserGroupError(MissingError):
    """Raised when a user group entry is missing."""
